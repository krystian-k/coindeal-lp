<section class="join relative">
  <img src="assets/images/ride.jpg"
      srcset="assets/images/ride@2x.jpg 2x,
      assets/images/ride@3x.jpg 3x"
      class="ride"
      data-aos="fade-in" 
      data-aos-duration="500" 
      data-aos-delay="200" 
      data-aos-once="true"
      alt="join the ride">
  <div class="wrapper" 
    data-aos="fade-left" 
    data-aos-duration="500" 
    data-aos-delay="700" 
    data-aos-once="true">
    <h2 class="join-title">join the ride</h2>
    <p class="join-description">
      Cryptocurrencies are here, and are making a statement. Make use of the trends, play the market and reap the rewards. We offer
      you a platform that exceeds all your needs and expectations. Just check out some of our
      <a href="#features"><span class="join-description-underline">unique features</span></a>.
    </p>
  </div>
</section>