var gulp = require("gulp");
var imagemin = require("gulp-imagemin");

module.exports = function() {
  gulp.src('src/assets/images/**/*')
    .pipe(imagemin([
      imagemin.gifsicle({interlaced: true}),
      imagemin.jpegtran({progressive: true}),
      imagemin.optipng({optimizationLevel: 5}),
      imagemin.svgo({plugins: [{removeViewBox: true}]})
    ]))
    .pipe(gulp.dest('dist/assets/images'))
}