var gulp = require("gulp");

var clean = require('./gulp/clean');
var index = require('./gulp/index');
var scripts = require('./gulp/scripts');
var styles  = require('./gulp/styles');
var listen = require('./gulp/listen');
var images = require('./gulp/images');
var fonts = require('./gulp/fonts');

gulp.task('clean', clean);
gulp.task('fonts', fonts);
gulp.task('index', index);
gulp.task('styles', styles);
gulp.task('scripts', scripts);
gulp.task('listen', listen);
gulp.task('imgmin', images);
gulp.task("watch", [
  "listen",
  "default"
]);

gulp.task("dist", [
  "index",
  "fonts",
  "styles",
  "scripts"
]);

gulp.task("default", ["dist"]);
