<footer class="page-footer">
  <div class="wrapper">
    <nav class="footer-nav">
      <div class="nav-logo">
        <img class="footer-logo" 
          src="assets/images/logo-footer.png" 
          srcset="assets/images/logo-footer@2x.png 2x,
            assets/images/logo-footer@3x.png 3x" 
          alt="coindeal logo footer">
      </div>
      <ul class="nav-list">
        <li class="nav-item">
          <a href="https://www.facebook.com/CoinDeal-154323511807081/" class="nav-link nav-link--circle">
            <i class="fa fa-facebook" aria-hidden="true"></i>
          </a>
        </li>
        <!-- <li class="nav-item">
          <a href="#" class="nav-link nav-link--circle">
            <i class="fa fa-twitter" aria-hidden="true"></i>
          </a>
        </li> -->
        <!-- <li class="nav-item nav-item--lang">
          <a href="#" class="nav-link">
            <img src="assets/images/language.png" 
              srcset="assets/images/language@2x.png 2x,
                assets/images/language@3x.png 3x"
              alt="poland flag icon">
          </a>
        </li> -->
        <li class="nav-item nav-item--lang">
          <a href="#" class="nav-link">
            <img src="assets/images/language-gb.png" 
              alt="british flag icon"
              class="flag-icon">
          </a>
        </li>
      </ul>
    </nav>
    <p class="footer-copyright">Copyright © 2017 <strong>Exchange Technologies</strong>. All rights reserved.</p>
  </div>
</footer>