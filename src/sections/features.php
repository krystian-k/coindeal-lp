<section id="features" class="features relative" 
  data-aos="fade-zoom-in"  
  data-aos-easing="ease-in-sine" 
  data-aos-duration="500" d
  ata-aos-delay="200" 
  data-aos-once="true">
  <div class="wrapper">
    <h2 class="features-title">Some of our exchange unique features:</h2>
    <div class="owl-carousel features-slider">

      <div class="slider-item">
        <div class="slider-item-img">
          <img src="assets/images/lowspread.png"
            srcset="assets/images/lowspread@2x.png 2x,
             assets/images/lowspread@3x.png 3x"
            alt="low spreads, high liquidity">
        </div>
        <div class="slider-item-title">Low spreads</div>
        <div class="slider-item-description">Guaranteed spreads, ultra high liquidity</div>
      </div>

      <div class="slider-item">
        <div class="slider-item-img">
          <img src="assets/images/manycryptcurrencies.png"
            srcset="assets/images/manycryptcurrencies@2x.png 2x,
            assets/images/manycryptcurrencies@3x.png 3x"
            alt="cryptocurrencies">
        </div>
        <div class="slider-item-title">Many<br>cryptocurrencies</div>
        <div class="slider-item-description">Bitcoin, Ethereum,
          <br>Dash, Bitcoin Cash
          <br>and others</div>
      </div>

      <div class="slider-item">
        <div class="slider-item-img">
          <img src="assets/images/manyfiat.png"
            srcset="assets/images/manyfiat@2x.png 2x,
            assets/images/manyfiat@3x.png 3x"
            alt="many FIAT currencies">
        </div>
        <div class="slider-item-title">Many FIAT<br>currencies</div>
        <div class="slider-item-description">EUR, USD, PLN and more!</div>
      </div>

      <div class="slider-item">
        <div class="slider-item-img">
          <img src="assets/images/ultralowfees.png"
            srcset="assets/images/ultralowfees@2x.png 2x,
            assets/images/ultralowfees@3x.png 3x"
            alt="ultra low fees">
        </div>
        <div class="slider-item-title">Ultra low fees</div>
        <div class="slider-item-description">For our early adopters!</div>
      </div>

      <div class="slider-item">
        <div class="slider-item-img">
          <img src="assets/images/uptime-ic.png"
            srcset="assets/images/uptime-ic@2x.png 2x,
            assets/images/uptime-ic@3x.png 3x"
            alt="uptime 100%">
        </div>
        <div class="slider-item-title">100% uptime</div>
        <div class="slider-item-description">No matter what</div>
      </div>

      <div class="slider-item">
        <div class="slider-item-img">
          <img src="assets/images/transparency.png"
            srcset="assets/images/transparency@2x.png 2x,
            assets/images/transparency@3x.png 3x"
            alt="ultimate transparency">
        </div>
        <div class="slider-item-title">Ultimate<br>transparency</div>
        <div class="slider-item-description">Deposited funds sum visible online</div>
      </div>

      <div class="slider-item">
        <div class="slider-item-img">
          <img src="assets/images/realtime.png"
            srcset="assets/images/realtime@2x.png 2x,
            assets/images/realtime@3x.png 3x"
            alt="real time auditing">
        </div>
        <div class="slider-item-title">Real time<br>auditing</div>
        <div class="slider-item-description">No possibility for single bitcoin/dollar missing</div>
      </div>

      <div class="slider-item">
        <div class="slider-item-img">
          <img src="assets/images/company.png"
            srcset="assets/images/company@2x.png 2x,
            assets/images/company@3x.png 3x"
            alt="company accounts">
        </div>
        <div class="slider-item-title">Company accounts</div>
        <div class="slider-item-description">Lower fees, better security, multi person transaction signing. Individual priority support, dedicated salesperson. </div>
      </div>

    </div>
  </div>
</section>