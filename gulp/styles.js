var gulp = require('gulp');
var rename = require("gulp-rename");
var sass = require('gulp-sass');
var autoprefixer = require("gulp-autoprefixer");
var clean_css = require("gulp-clean-css");

module.exports = function() {
  gulp.task("styles", function() {
    return gulp.src("./src/assets/scss/*.{scss,sass}")
      .pipe(sass({
        outputStyle: "expanded",
        sourceComments: true
      }))
      .on('error', sass.logError)
      .pipe(autoprefixer({
        browsers: ["last 2 version", "> 1%"]
      }))
      .pipe(clean_css({level:2}))
      .pipe(rename(function(path) {
        path.extname = ".min.css"
      }))
      .pipe(gulp.dest("dist/assets/css"))
  })
}