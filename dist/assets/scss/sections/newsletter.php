<section class="newsletter relative">
  <!-- <div class="shape"></div> -->
  <div class="wrapper" 
    data-aos="fade-up" 
    data-aos-offset="200" 
    data-aos-easing="ease-in-sine" 
    data-aos-duration="500" 
    data-aos-delay="400" 
    data-aos-once="true"
  >
    <h3 class="section-title section-title--newsletter">Sign up now and get lower fees!</h3>
    <p class="section-subtitle">Enjoy
      <strong>10% discount</strong> on fees till the end of 2018.</p>
    <form class="newsletter-form" action="">
      <input class="newsletter-form__input newsletter-form__email" type="email" name="email" id="email" placeholder="Your e-mail address here"
        required>
      <input class="newsletter-form__input newsletter-form__btn" type="submit" value="sign up">
    </form>
  </div>
</section>