<section id="newsletter" class="newsletter relative">  
  <div class="wrapper" 
    data-aos="fade-up" 
    data-aos-easing="ease-in-sine" 
    data-aos-duration="500" 
    data-aos-delay="200" 
    data-aos-once="true"> 
    <h3 class="newsletter-title">Sign up now and get lower fees!</h3>
    <p class="newsletter-subtitle">Enjoy <strong>10% discount</strong> on fees till the end of 2018.</p>
    <form class="newsletter-form" action="post">
      <input class="newsletter-form-input newsletter-form-email" type="email" name="email" id="email" placeholder="Your e-mail address here"
        required>
      <input class="newsletter-form-input newsletter-form-btn newsletter-form-submit" type="submit" value="sign up">
      <div class="agreement"><input type="checkbox" name="agree" required>I agree to receive the newsletter</div>
    </form>
  </div>
</section>