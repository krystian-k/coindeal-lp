var gulp = require('gulp');

module.exports = function () {
  gulp.task('fonts', function() {
    return gulp.src('src/assets/fonts/*')
      .pipe(gulp.dest('dist/assets/fonts'));
  });
}