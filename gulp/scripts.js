var gulp = require('gulp');
var rename = require("gulp-rename");
var uglify = require("gulp-uglify");
var babel = require("gulp-babel");

module.exports = function() {
  gulp.task("scripts", function() {
    return gulp.src("src/assets/js/**/*.js")
      .pipe(babel({
        presets: ['es2015']
      }))
      .pipe(uglify())
      .pipe(rename(function(path) {
        path.extname = ".min.js"
      }))
      .pipe(gulp.dest("dist/assets/js"));
  });
}
