var gulp = require('gulp');

module.exports = function() {
  gulp.task("index", function() {
    return gulp.src("src/**/*.php")
      .pipe(gulp.dest("dist/"))
  });
}