<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Coindeal</title>
 
  <meta name="description" content="Trade bitcoin, ethereum and litecoin in exchange with UDS, EUR, PLN. Best btc price on the coin market with quick deposit and withdraw from coinbase wallet, bank transfer, credit card.">
  <meta name="keywords" content="bitcoin, ethereum, dash, exchange, buy, sell, trade, trading, BTC, ETH, XMR, LTC, DASH, EUR, USD, PLN">
  <meta name="og:title" content="Coin Deal – buy, trade and change bitcoin, ethereum and dash on cryptocurrency exchange.">
  <meta name="og:description" content=" Trade bitcoin, ethereum and litecoin in exchange with UDS, EUR, PLN. Best btc price on the coin market with quick deposit and withdraw from coinbase wallet, bank transfer, credit card.">
  <meta name="og:url" content="http://www.coindeal.com/">
  <meta name="og:site_name" content=" Coin Deal – buy, trade and change bitcoin, ethereum and dash on cryptocurrency exchange">
  <meta name="og:type" content="website">
  <meta name="robots" content="index,follow"/>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  
  <!-- Animations -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.2.0/aos.css">
  
  <!-- Carousel -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css">
  
  <!-- Main stylesheet -->
  <link rel="stylesheet" href="assets/css/main.min.css">
</head>

<body>
  <?php require_once('common/page-header.php'); ?>
  <main class="page-content">
    <?php require_once('sections/join.php'); ?>
    <?php require_once('sections/security.php'); ?>
    <?php require_once('sections/features.php'); ?>
    <?php require_once('sections/newsletter.php'); ?>
  </main>
  <?php require_once('common/page-footer.php'); ?>

  <!-- Carousel -->
  <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>
  <script>
    $(document).ready(function () {
      $(".features-slider").owlCarousel({
        items: 4,
        nav: true,
        dots: true,
        loop: true,
        navText: '◂▸',
        autoHeight: true,
        responsiveClass:true,
        responsive:{
          0:{
            items: 1,
            nav: true
          },
          600:{
            items: 1,
            nav: true
          },
          1170:{
            items: 4,
            nav: true,
          }
        }
      });
    });
  </script>

  <!-- Animations -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.2.0/aos.js"></script>
  <script>
    AOS.init();
  </script>

  <script>
    $(".newsletter-form").submit("click",function () {
      var url = "./save-email";
      
      e.preventDefault();
      $.ajax({
        type: "POST",
        url: url,
        data: $(".newsletter-form").serialize(),
        success: function(data) { 
          console.log('You have just joined the elite group of people making cryptocurrency market history. Good decision! See you soon in the fastest cryptocurrency exchange in the world');
        }
      });
    });

    $('a[href*=\\#]').on('click', function(event){     
      event.preventDefault();
      $('html,body').animate({scrollTop:$(this.hash).offset().top}, 800);
    });
  </script>
</body>

</html>