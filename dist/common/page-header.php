<header class="page-header relative">
  <nav class="header-nav">
    <div class="nav-logo">
      <img class="header-logo" 
        src="assets/images/logoheader.png" 
        srcset="assets/images/logoheader@2x.png 2x,
          assets/images/logoheader@3x.png 3x" 
        alt="coindeal logo">
    </div>
    <ul class="nav-list">
      
      <li class="nav-item nav-item--btn">
        <a href="#newsletter" class="nav-link nav-link--square">sign up</a>
      </li>
      
      <li class="nav-item">
        <a href="#" class="nav-link nav-link--circle">
          <i class="fa fa-facebook" aria-hidden="true"></i>
        </a>
      </li>

      <!-- <li class="nav-item">
        <a href="#" class="nav-link nav-link--circle">
          <i class="fa fa-twitter" aria-hidden="true"></i>
        </a>
      </li> -->

      <!-- <li class="nav-item nav-item--lang">
        <a href="#" class="nav-link">
          <img src="assets/images/language.png" 
            srcset="assets/images/language@2x.png 2x,
              assets/images/language@3x.png 3x"
            alt="poland flag icon">
        </a>
      </li> -->

      <li class="nav-item nav-item--lang">
        <a href="#" class="nav-link">
          <img src="assets/images/language-gb.png" 
            alt="british flag icon"
            class="flag-icon">
        </a>
      </li>
    </ul>
  </nav>
    <div class="wrapper">
      <div class="header-title-wrapper"
        data-aos="fade-up" 
        data-aos-anchor-placement="top-center"
        data-aos-duration="500"
        data-aos-delay="200"
        data-aos-once="true">
        <p class="header-title">it's time</p>
        <p class="header-title-big">for
          <br>a change.</p>
        <h2 class="header-subtitle">new, innovative cryptocurrency exchange is coming!</h2>
      </div>
      <div class="header-form-wrapper"
        data-aos="fade-zoom-in"
        data-aos-easing="ease-in-sine"
        data-aos-duration="500"
        data-aos-delay="600"
        data-aos-once="true">
        <form class="header-form newsletter-form" action="">
          <input class="header-form-input header-form-email" type="email" name="email" id="email" placeholder="Your e-mail address here"
            required>
          <input class="header-form-input header-form-btn newsletter-form-submit" type="submit" value="sign up">
          <div class="agreement"><input type="checkbox" name="agree" required>I agree to receive the newsletter</div>
        </form>
      </div>
    </div>
  </header>