<section class="security relative">
  <img src="assets/images/safe.jpg"
      srcset="assets/images/safe@2x.jpg 2x,
      assets/images/safe@3x.jpg 3x"
      class="safe"
      data-aos="fade-in" 
      data-aos-duration="500" 
      data-aos-delay="200" 
      data-aos-once="true"
      alt="safe storage">
  <div class="wrapper" 
    data-aos="fade-right" 
    data-aos-duration="500" 
    data-aos-delay="700" 
    data-aos-once="true">
    <h2 class="security-title">absolute security</h2>
    <p class="security-description">Cold storage - encrypted keys<br>deposited in Swiss banks.</p>
  </div>
</section>