var gulp = require('gulp');
var del = require("del");

module.exports = function() {
  gulp.task("clean", function () {
    del.sync([
      "dist/"
    ]);
  });
}